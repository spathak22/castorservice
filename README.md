# CastorService

## Installation
#####Perform following steps under the CastorService directory

Update the paths in Castorclient.java

```sh
    home: Provide the path to CastorWebApp
```

Install Castor.jar

```sh
    mvn install:install-file -Dfile=dependencies/Castor.jar -DgroupId=Castor -DartifactId=Castor -Dversion=1.0 -Dpackaging=jar
```

Install mymln2poss.jar
 	
```sh
    mvn install:install-file -Dfile=dependencies/mymln2poss.jar -DgroupId=mymln2poss -DartifactId=mymln2poss -Dversion=1.0 -Dpackaging=jar
```

Run maven build 

```sh
    mvn clean install 
```

Run the spark REST service

```sh
    nohup java -cp target/CastorService-1.0-SNAPSHOT.jar CastorService & 
```
The spark service and castor logs will be written "nohup.out" file

