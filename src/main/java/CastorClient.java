import castor.wrappers.LearningResult;
import castor.clients.CastorCmd;

public class CastorClient {

	//public static final String home="/nfs/stak/users/pathaks/public_html/icde18-demo";
	public static final String home = "/Users/SherLock/Desktop/Books/REsearch/Code/Castor/web-app/CastorWebApp";

	public LearningResult learn(String dataset, String method, String query, boolean createSPs, boolean pos, boolean neg) {
		String parameters;
		String inds;
		String dataModel;
		String trainPosSuffix;
		String trainNegSuffix;
		String testPosSuffix;
		String testNegSuffix;
		String posTrainExamplesFile = null;
		String negTrainExamplesFile = null;
		String posTestExamplesFile = null;
		String negTestExamplesFile = null;

		String ddl = null;

		if (dataset.equals("uwcse")) {
			if (createSPs)
				parameters = "/Users/jose/Box Sync/castor-demo/uwcse/Baseline/castor-input/parameters.json";
			else
				parameters = "/Users/jose/Box Sync/castor-demo/uwcse/Baseline/castor-input/parameters-noSPs.json";

			inds = "/Users/jose/Box Sync/castor-demo/uwcse/Baseline/castor-input/inds-schema1.json";

			if (method.equals("baseline"))
				dataModel = "/Users/jose/Box Sync/castor-demo/uwcse/Baseline/castor-input/datamodel-schema1.json";
			else if (method.equals("manual"))
				dataModel = "/Users/jose/Box Sync/castor-demo/uwcse/ManualTuned/castor-input/datamodel-schema1.json";
			else
				dataModel = "/Users/jose/Box Sync/castor-demo/uwcse/AutoMode/castor-input/datamodel-schema1.json";

			trainPosSuffix = "_FOLD2_TRAIN_POS";
			trainNegSuffix = "_FOLD2_TRAIN_NEG";
			testPosSuffix = "_FOLD2_TEST_POS";
			testNegSuffix = "_FOLD2_TEST_NEG";
		}else if(dataset.equals("immortals")){
			System.out.println("Dataset immortals query "+ method);

			parameters = home+"/modes/immortals/"+query+"/castor-input/parameters.json";
			System.out.println(parameters);
			inds = home+"/modes/immortals/"+query+"/castor-input/inds.json";
			System.out.println(inds);

			dataModel = home+"/modes/immortals/"+query+"/castor-input/"+method+"/datamodel.json";
			System.out.println(dataModel);

			if(pos){
				posTrainExamplesFile=home+"/modes/immortals/"+query+"/examples/out_all_pos.csv";
				System.out.println(posTrainExamplesFile);

			}else{
				posTrainExamplesFile=home+"/modes/immortals/"+query+"/examples/"+query+"_all_pos.csv";
				System.out.println(posTrainExamplesFile);
			}

			if(neg){
				negTrainExamplesFile=home+"/modes/immortals/"+query+"/examples/out_all_neg.csv";
				System.out.println(negTrainExamplesFile);
			}else {
				negTrainExamplesFile=home+"/modes/immortals/"+query+"/examples/"+query+"_all_neg.csv";
				System.out.println(negTrainExamplesFile);
			}

			posTestExamplesFile=home+"/modes/immortals/"+query+"/test/"+query+"_all_pos.csv";
			System.out.println(posTestExamplesFile);
			negTestExamplesFile=home+"/modes/immortals/"+query+"/test/"+query+"_all_neg.csv";
			System.out.println(negTestExamplesFile);


			trainPosSuffix = "_TRAIN_POS";
			trainNegSuffix = "_TRAIN_NEG";
			testPosSuffix = "_TEST_POS";
			testNegSuffix = "_TEST_NEG";

		}


		else {
			throw new IllegalArgumentException("Dataset not supported.");
		}
		
		String[] args = {
				"-parameters", parameters,
				"-inds", inds,
				"-dataModel", dataModel,
				"-posTrainExamplesFile", posTrainExamplesFile,
				"-negTrainExamplesFile", negTrainExamplesFile,
				"-outputSQL", "true",
				"-posTestExamplesFile", posTestExamplesFile,
				"-negTestExamplesFile", negTestExamplesFile,
				"-test",
				};
		
		CastorCmd program = new CastorCmd();
		LearningResult learningResult = program.run(args);

		return learningResult;
	}
}
